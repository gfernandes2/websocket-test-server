exports.emitToRoom = (namespace, roomIdentifier, eventName, data) => {
  console.log(`Emit ${eventName}`, roomIdentifier, namespace, data);
  const response = global.io.of(namespace).to(`${roomIdentifier}`).emit(eventName, data);
  // console.log("TCL: exports.emitToRoom -> response", response)
};

exports.emitOverChannel = (namespace, eventName, data) => {
  console.log(`Emit over channel ${eventName}`, data);
  global.io.of(namespace).emit(eventName, data);
};

exports.init = async namespace => {
  global.io.of(namespace).on("connection", async socket => {
    // const query = socket.request._query;

    socket.on("subscribe_to_room", data => {
      socket.join(data.room);
    });
  });
};
