const express = require("express");
const socketRoutes = require("./socket/routes");

const router = express.Router();

/**
 * GET v1/status
 */
router.get("/status", (req, res) => res.send("OK"));
/**
 * GET v1/socket
 */
router.use("/socket", socketRoutes);

module.exports = router;
