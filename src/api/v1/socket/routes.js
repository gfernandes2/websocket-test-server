const express = require("express");
const controller = require("./controller");

const routes = express.Router();

routes.route("/notifications").get(controller.notification);

routes.route("/messages").get(controller.messages);

routes.route("/preauction").get(controller.preauction);

routes.route("/auctionUpdate").get(controller.auctionUpdate);

routes.route("/auction").get(controller.auction);

module.exports = routes;
