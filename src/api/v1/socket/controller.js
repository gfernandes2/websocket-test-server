/* eslint-disable max-lines */
const httpStatus = require("http-status");
const faker = require("faker");
const socket = require("../../../socket");

const types = ["announcement", "reminders", "messages"];
const userTypes = ["leadArranger", "legalCounselServiceProvider"];

// Oliver Queen
// danny crane

const userIds = [
  "a4b0c646-add1-4e19-8463-b019671a9b0a",
  "3b49bd05-d8bb-4d4f-b897-e1c7dc571585",
];

const investorIds = [
  "b1a9c666-cfe4-4a2d-9d77-e782930e67f7",
  "c9190061-5e41-4e26-86ff-dadf2527bf8f",
];

const bidIds = [
  "1e364cfe-1ad7-49db-a2fc-b5e26f9a8a57",
  "222bacd8-e8aa-477d-8475-d24a5d1c6a36",
];

// Oliver Queen X Danny Crane
// Random ID

const roomIds = [
  "1dbfc0b2-e2fe-4049-b9d6-c238f0ea7b82",
  "3b49bd05-d8bb-4d4f-b897-e1c7dc571585",
];

const sukukIds = ["c5c31052-87ba-4ea1-aa23-0bb4e595dca1"];

/**
 * Notification
 * @public
 */
exports.notification = async (req, res, next) => {
  try {
    const temp = {
      fromUser: {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      },
      id: faker.random.uuid(),
      message: faker.lorem.sentence(),
      notificationShowTime: faker.date.recent(),
      parse: faker.random.boolean(),
      read: faker.random.boolean(),
      type:
        types[
          faker.random.number({
            max: 2,
            min: 0,
          })
        ],
    };

    // socket.emitOverChannel("notification", temp);
    socket.emitToRoom(
      "socket/v1/notification",
      userIds[
        faker.random.number({
          max: 1,
          min: 0,
        })
      ],
      "notification",
      temp
    );

    return res.status(httpStatus.OK).json(temp);
  } catch (error) {
    return next(error);
  }
};

/**
 * Messages
 * @public
 */
exports.messages = async (req, res, next) => {
  try {
    const message = {
      id: faker.random.uuid(),
      message: faker.lorem.sentence(),
      type:
        userTypes[
          faker.random.number({
            max: 1,
            min: 0,
          })
        ],
      userId:
        userIds[
          faker.random.number({
            max: 1,
            min: 0,
          })
        ],
    };

    socket.emitToRoom(
      "socket/v1/message",
      roomIds[
        faker.random.number({
          max: 0,
          min: 0,
        })
      ],
      "messages",
      message
    );

    return res.status(httpStatus.OK).json(message);
  } catch (error) {
    return next(error);
  }
};

/**
 * preauction
 * @public
 */
exports.preauction = async (req, res, next) => {
  try {
    const message = {
      bids: [
        {
          id: faker.random.uuid(),
          corporateEntityName: "Investor Two",
          bid: faker.random.number(),
          modifications: {
            issuanceSize: {
              value: faker.random.number(),
              message: faker.lorem.sentence(),
            },
            profitFrequency: null,
            profitRateBenchmark: null,
            maturityDate: null,
          },
          variant: "d4a33275-a9e4-4c14-ae34-884b471d6e4c",
        },
        {
          id: faker.random.uuid(),
          corporateEntityName: "Investor Two",
          bid: faker.random.number(),
          modifications: {
            issuanceSize: null,
            profitFrequency: null,
            profitRateBenchmark: {
              value: faker.random.number(),
              message: faker.lorem.sentence(),
            },
            maturityDate: null,
          },
          variant: "b41550fa-b481-4a6f-b941-d20d79850939",
        },
        {
          id: faker.random.uuid(),
          corporateEntityName: "Investor One",
          bid: faker.random.number(),
          modifications: {
            issuanceSize: null,
            profitFrequency: {
              value: faker.random.number(),
              message: faker.lorem.sentence(),
            },
            profitRateBenchmark: null,
            maturityDate: null,
          },
          variant: "b41550fa-b481-4a6f-b941-d20d79850939",
        },
      ],
      investors: [
        {
          id: "5a750d45-6aec-4252-9fd1-59e05c73432f",
          user_id: "b1a9c666-cfe4-4a2d-9d77-e782930e67f7",
          corporateEntityName: "Investor One",
        },
        {
          id: "8af281e4-03c9-43b2-bbed-020ff4111ba8",
          user_id: "c9190061-5e41-4e26-86ff-dadf2527bf8f",
          corporateEntityName: "Investor Two",
        },
      ],
      variants: [
        {
          id: "d4a33275-a9e4-4c14-ae34-884b471d6e4c",
          sukukId: "3d2dc4db-f999-42d2-8679-0cc55232eeb2",
          originalIssuanceSize: 1000,
          originalCouponRate: "2.5500000000",
          originalProfitRateBenchmark: null,
          originalProfitFrequency: "Daily",
          originalMaturityDate: "2020-01-25T12:37:45.000Z",
          modifiedIssuanceSize: null,
          modifiedCouponRate: null,
          modifiedProfitRateBenchmark: null,
          modifiedProfitFrequency: null,
          modifiedMaturityDate: null,
        },
        {
          id: "b41550fa-b481-4a6f-b941-d20d79850939",
          sukukId: "3d2dc4db-f999-42d2-8679-0cc55232eeb2",
          originalIssuanceSize: 10000,
          originalCouponRate: "1.5500000000",
          originalProfitRateBenchmark: null,
          originalProfitFrequency: "Daily",
          originalMaturityDate: "2020-01-25T12:37:45.000Z",
          modifiedIssuanceSize: null,
          modifiedCouponRate: null,
          modifiedProfitRateBenchmark: null,
          modifiedProfitFrequency: null,
          modifiedMaturityDate: null,
        },
      ],
      status: "Ongoing",
    };

    socket.emitToRoom(
      "socket/v1/preAuction/bids",
      sukukIds[
        faker.random.number({
          max: 0,
          min: 0,
        })
      ],
      "preauction",
      message
    );

    return res.status(httpStatus.OK).json(message);
  } catch (error) {
    return next(error);
  }
};



/**
 * auction
 * @public
 */
exports.auction = async (req, res, next) => {
  try {
    const message = {
      bidId: faker.random.uuid(),
      sukukId: sukukIds[faker.random.number({max: 0,min: 0,})],
      userId: investorIds[faker.random.number({max: 1,min: 0,})],
      profitRate: parseFloat(faker.random.number({max: 0,min: 9})+'.'+ faker.random.number({max: 10,min: 99})),
      bid: faker.random.number({max:786}),
      allocatedBidAmount: faker.random.number({max:786}),
    };

    socket.emitToRoom(
      "socket/v1/auction/bids",
      sukukIds[
        faker.random.number({
          max: 0,
          min: 0,
        })
      ],
      "New",
      message
    );

    return res.status(httpStatus.OK).json(message);
  } catch (error) {
    return next(error);
  }
};


exports.auctionUpdate = async (req, res, next) => {
  try {
    const message = {
      userId: investorIds[faker.random.number({max: 1,min: 0,})],
      bidId: bidIds[
        faker.random.number({
          max: 1,
          min: 0,
        })
      ],
      bid: faker.random.number({max:786}),
      sukukId: sukukIds[faker.random.number({max: 0,min: 0,})],
      profitRate: parseFloat(faker.random.number({max: 0,min: 9})+'.'+ faker.random.number({max: 10,min: 99})),
      allocatedBidAmount: faker.random.number({max:786}),
    };

    socket.emitToRoom(
      "socket/v1/auction/bids",
      sukukIds[
        faker.random.number({
          max: 0,
          min: 0,
        })
      ],
      "Update",
      message
    );

    return res.status(httpStatus.OK).json(message);
  } catch (error) {
    return next(error);
  }
};

