// eslint-disable-next-line no-global-assign
Promise = require("bluebird");
const httpServer = require("http").createServer();
const {
  port, env, socketUrl, socketPort
} = require("./config");

const server = require("./server");

const socket = require("./socket");

global.io = require("socket.io").listen(httpServer);

socket.init("socket/v1/notification");
socket.init("socket/v1/message");
socket.init("socket/v1/preAuctionBids");
socket.init("socket/v1/auction/bids");

// database.connect();

server.listen(port, () => {
  console.info(`Server started on port ${port} (${env})`);
});

httpServer.listen(socketPort, socketUrl, () => {
  console.info(`Socket server started on ${socketUrl}:${socketPort}(${env})`);
});

const src = server;

module.exports = src;
