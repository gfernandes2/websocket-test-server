module.exports = {
  extends: 'imbudhiraja',
  rules: {
    'linebreak-style': 'off',
    'no-console': 'off',
    'no-underscore-dangle': 'off',
    'filenames/match-exported': [2, 'kebab'],
    "quotes": [2, "double", { "avoidEscape": true }],
    "arrow-parens": ["error", "as-needed"],
    "comma-dangle": ["error", "never"],
  },
};
