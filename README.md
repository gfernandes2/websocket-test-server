## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```bash
#!/bin/bash
Node@v10.x.x
```

### Useful Scripts

| Script | Description |
| ------ | ------ |
| npm start | Starts development server at localhost:3001 and socket server at localhost:3002 |

### Run at local server

```bash
touch .env
nano .env
NODE_ENV=development

npm run start
http://localhost:3001/
```

### Check Server Status
Once server has started run http://localhost:3001/v1/status.

### Websockets Test

Log in with Oliver Queen and Denny Crane ( Their user ids are hard coded )

#### Notifications

To send notifications call this url, it will randomly send a generated notification to either Oliver Queen or Denny Crane. Both will be connected to the ```/notification``` namespace and rooms using their own ```userID``` 

```http://localhost:3001/v1/socket/notifications```

#### Messages

To check websockets between the 2 users open the chat boxes between them (```Superman Rebirth Project```) and call the link below. These messages are generated and are not connected to the db.

```http://localhost:3001/v1/socket/messages```

Known Issue
The /messages does not connect on the first mount of the chatbox. You will need to close the chat box and open it again to start getting the messages.


